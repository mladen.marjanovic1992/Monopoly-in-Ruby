class Color

	attr_reader :colorName, :fields, :owner

	def initialize (colorName, fields)
		@colorName = colorName
		@fields = fields
		@owner = nil
	end

	def ownedBy?(player)
		@fields.all? { |field| field.owner == player }
	end

	def setOwner(player) # player attribute is an object of the Player class
		@owner = player
		unless hasHouses?
			@fields.each do |field|
				field.currentRent = field.rent[1]
			end
		end
	end

	def hasHouses?
		@fields.any? { |field| field.houses > 0 } ? true : false
	end

end
