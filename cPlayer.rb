class Player

  attr_reader :position, :name, :funds, :fieldsOwned, :colorsOwned
  attr_accessor :fieldsUnderMortgage, :jail_exit_cards, :rollsUntilOut

  def initialize (funds = 0, _name, position)
    @position = position
    @funds = funds
    @jail_exit_cards = 0
    @name = _name
    @fieldsOwned = []
    @fieldsUnderMortgage = []
    @colorsOwned = []
    @rollsUntilOut = 0
  end

  def setPosition(field)
    @position = field
  end

  def inJail?
    @rollsUntilOut != 0 ? true : false
  end

  def addField(field)
    unless @fieldsOwned.include?(field)
      @fieldsOwned << field
      if field.underMortgage
        @fieldsUnderMortgage << field
      end
    end
  end

  def removeField(field)
    @fieldsOwned.delete(field)
    @fieldsUnderMortgage.delete(field)
  end

  def addColor(color)
    unless @colorsOwned.include?(color)
      @colorsOwned << color
    end
  end

  def removeColor(color)
    @colorsOwned.delete(color)
    color.setOwner(nil)
  end

  def changeFunds(ammount)
    @funds += ammount
  end

  def remainingAssets
    remainingAssets = 0
    (@fieldsOwned.select { |f| !f.underMortgage }).each do |field|
      remainingAssets += (field.mortgage + (field.houses * field.houseCost) / 2)
    end
    return remainingAssets
  end

end
