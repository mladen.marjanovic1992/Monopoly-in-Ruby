require './cField.rb'
require './cPlayer.rb'
require './cColor.rb'
require './cMap.rb'
require './cGame.rb'
require "rubygems"
require "highline/import"

class Interface

  attr_reader :game, :fields, :players, :bankruptedByBank, :bankruptedByPlayer

  def initialize(game)
    @fields = game.map.fields
    @colors = game.map.colors
    @board = game.map.board
    @players = game.players
    @game = game
  end

  def showPlayerStats(player)
    puts "Name: #{player.name}"
    puts "Funds: #{player.funds.to_i}"

    print "Owns fields: "
    player.fieldsOwned.each do |field|
      print "#{field.fieldName}; "
    end

    print "\nOwns colors: "
    player.colorsOwned.each do |color|
      print "#{color.colorName.capitalize}; "
    end

    print "\nFields under mortgage: "
    player.fieldsUnderMortgage.each do |field|
      print "#{field.fieldName}: "
    end

    puts "\nJail cards: #{player.jail_exit_cards}"
    puts "Position: #{player.position.fieldName}"
    if player.inJail?
      puts "Doing hard time... #{player.rollsUntilOut} throws until out of jail."
    end

  end

  def showFieldStats(propertyName)
    @fields.each do |field|
      if (field.fieldName == propertyName ) && (field.property?)
        puts "Name: #{field.fieldName}"
        puts "Color: #{field.fieldColor}"
        puts "Price: #{field.price}"
        field.owner != nil ? (puts "Owner: #{field.owner.name}") : (print "")
        field.houses <= 4 ? (puts "Buildings: #{field.houses} houses") : (puts "Buildings: Hotel")
        puts "Rent: #{field.rent}"
        puts "Current rent: #{field.currentRent}"
        puts "Mortgage: #{field.mortgage}"
      elsif (field.fieldName == propertyName) && (field.station? || field.utility?)
        puts "Name: #{field.fieldName}"
        puts "Price: #{field.price}"
        field.owner != nil ? (puts "Owner: #{field.owner.name}") : (print "")
        puts "Rent: #{field.rent}"
        puts "Current rent: #{field.currentRent}"
        puts "Mortgage: #{field.mortgage}"
      end
    end
  end

  def showMap
    print @board
  end

end



map1 = Map.new

brown = Color.new("brown", [map1.fields[1], map1.fields[3]])
lightBlue = Color.new("lightBlue", [map1.fields[6], map1.fields[8], map1.fields[9]])
pink = Color.new("pink", [map1.fields[11], map1.fields[13], map1.fields[14]])
orange = Color.new("orange", [map1.fields[16], map1.fields[18], map1.fields[19]])
red = Color.new("red", [map1.fields[21], map1.fields[23], map1.fields[24]])
yellow = Color.new("yellow", [map1.fields[26], map1.fields[27], map1.fields[29]])
green = Color.new("green", [map1.fields[31], map1.fields[32], map1.fields[34]])
darkBlue = Color.new("darkBlue", [map1.fields[37], map1.fields[39]])

map1.colors.push(brown, lightBlue, pink, orange, red, yellow, green, darkBlue)

puts "\nWelcome to Monopoly - By Djomla!"

numberOfPlayers = 0
numberOfPlayers = ask("\nHow many players? (2-6)", Integer)  { |q| q.in = 2..6 }
players = []
counter = 1
numberOfPlayers.to_i.times do
  playerName = ask("\nEnter name for player #{counter}: ", String)
  players << Player.new(2000, playerName, map1.fields[0])
  counter += 1
end

puts "\nThe game has started. Good luck!\n"

game1 = Game.new(map1, players)

int1 = Interface.new(game1)


# begin main loop
@gameOver = false
@alreadyRolled = false
until @gameOver do

  say("\n\nWhat would you like to do?")

  choose do |menu| # main menu for player actions

    unless @alreadyRolled
      menu.choice("Roll") do
        rolled = int1.game.roll
        if !int1.game.currentPlayer.inJail? || int1.game.rolledDouble?
          int1.game.movePlayer(rolled)
          int1.game.currentPlayer.rollsUntilOut = 0
          int1.game.effect
        else
          puts "Still in jail!"
          int1.game.currentPlayer.rollsUntilOut -= 1
        end
        @alreadyRolled = true
      end
    end

    unless (int1.game.currentPlayer.position.owned?) || !(int1.game.currentPlayer.position.property? || int1.game.currentPlayer.position.station? || int1.game.currentPlayer.position.utility?) || (int1.game.currentPlayer.position.price > int1.game.currentPlayer.funds)
      menu.choice("Buy") do
        if agree("Buy #{int1.game.currentPlayer.position.fieldName} for #{int1.game.currentPlayer.position.price}$? (y/n)")
          int1.game.buyOrAuction(int1.game.currentPlayer, int1.game.currentPlayer.position, int1.game.currentPlayer.position.price)
        end
      end
    end

    unless (int1.game.currentPlayer.position.owned?) || !(int1.game.currentPlayer.position.property? || int1.game.currentPlayer.position.station? || int1.game.currentPlayer.position.utility?)

      menu.choice("Auction") do # will need to make another auction option for bankrupcy auction

        @winningBid = ask("What was the winning bid?", Integer).to_i

        choose do |menu|

          say("Who won the auction?")

          players = int1.players.select { |player| player.funds >= @winningBid }.collect { |player| player.name }

          menu.choices(*players) do |chosen|
            @auctionWinner = int1.players.select { |player| chosen == player.name }[0]
          end

        end

        if agree("#{@auctionWinner.name.capitalize}, are you sure you want to buy #{int1.game.currentPlayer.position.fieldName} for #{@winningBid} dollars? (y/n)")
          int1.game.buyOrAuction(@auctionWinner, int1.game.currentPlayer.position, @winningBid)
        end

      end

    end

    if @alreadyRolled # can't end move without rolling the dice

      menu.choice("End move") do

        if int1.game.currentPlayer.funds >= 0
          int1.game.nextPlayer
          @alreadyRolled = false
        else

          if int1.game.currentPlayer.remainingAssets >= -(int1.game.currentPlayer.funds) # the player can settle his debt by mortgaging properties or selling houses
            puts "You have a negative balance. Sell houses or put properties under mortgage to settle your debts! Don't worry, you're not bankrupt!"
          elsif (int1.game.currentPlayer.position.property?) || (int1.game.currentPlayer.position.station?) || (int1.game.currentPlayer.position.utility?) # if bankrupted by another player
            int1.game.bankrupt { int1.game.trade(int1.game.currentPlayer.position.owner, 0, int1.game.overpaid, int1.game.currentPlayer.fieldsOwned, [], true) }
            if int1.game.players.length == 1
              puts "Game over! Congratulations, #{int1.game.players[0].name.capitalize}! You are the winner!"
              @gameover = true
              exit
            end
            int1.game.nextPlayer
            @alreadyRolled = false
          else # if bankrupted by bank
            int1.game.bankrupt{ int1.game.currentPlayer.fieldsOwned.each { |field| field.setOwner("Bank") } } # sets the "bank as a fictional owner until the field is auctioned off"
            if int1.game.players.length == 1
              puts "Game over! Congratulations, #{int1.game.players[0].name.capitalize}! You are the winner!"
              @gameover = true
              exit
            end

            puts "Properties owned by the bankrupt player are put on auction!"
            int1.fields.select { |field| field.owner == "Bank" }.each do |field|
              puts "Auction for #{field.fieldName}!"
              @winningBid = ask("What was the winning bid?", Integer).to_i
              choose do |menu|
                say("Who won the auction?")

                players = int1.players.select { |player| player.funds >= @winningBid }.collect { |player| player.name }

                menu.choices(*players) do |chosen|
                  @auctionWinner = int1.players.select { |player| chosen == player.name }[0]
                end

              end
              if agree("#{@auctionWinner.name.capitalize}, are you sure you want to buy #{field.fieldName} for #{@winningBid}$? (y/n)")
                int1.game.buyOrAuction(@auctionWinner, field, @winningBid)
              end
            end

            int1.game.nextPlayer
            @alreadyRolled = false
          end

        end

      end

    end

    menu.choice("Trade") do # for trade method

      @withPlayer = nil
      @offerFunds = 0
      @demandFunds = 0
      @offerFields = []
      @demandFields = []
      @confirm = false

      say("\nWhich player do you want to trade with?") # for player parameter in trade

      choose do |menu|

        otherPlayers = int1.players.select{ |player| player != int1.game.currentPlayer }.collect { |player| player.name }

        menu.choices(*otherPlayers) do |chosen|

          int1.players.each do |player|
            if chosen == player.name
              @withPlayer = player  # @withPlayer is the player object which will be used as a parameter in the trade method
            end
          end

        end

      end


      say("\nWhich fields do you demand?") # for demandFields parameter in trade

      doneDemanding = false
      until doneDemanding do

        choose do |menu| # menu for choosing fields you demand

          menu.choice("End selection") do
            doneDemanding = true
          end

          fieldsForDemanding = [] # contains the names of all fields owned by the player with which you want to trade (for displaying in the menu)
          @withPlayer.fieldsOwned.each do |field|
            unless ([field] - @demandFields == []) # so it doesn't display fields already chosen
              fieldsForDemanding << field.fieldName
            end
          end

          menu.choices(*fieldsForDemanding) do |field|
            @withPlayer.fieldsOwned.each do |fieldOwned|
              if field == fieldOwned.fieldName
                @demandFields << fieldOwned # @demandFields is an array of chosen fields to be used in the trade method
              end
            end
          end

        end

      end


      say("\nWhich fields do you offer?") # for offerFields parameter in trade

      doneOffering = false
      until doneOffering do

        choose do |menu|
          menu.choice("End selection") do
            doneOffering = true
          end

          fieldsForOffering = [] # contains the names of all fields owned by the player making the trade (for displaying in the menu)
          int1.game.currentPlayer.fieldsOwned.each do |field|
            unless ([field] - @offerFields == []) # so it doesn't display fields already chosen
              fieldsForOffering << field.fieldName
            end
          end

          menu.choices(*fieldsForOffering) do |field|
            int1.game.currentPlayer.fieldsOwned.each do |fieldOwned|
              if field == fieldOwned.fieldName
                @offerFields << fieldOwned # @offerFields is an array of chosen fields to be used in the trade method
              end
            end
          end

        end

      end


      @demandFunds = ask("How much money are you demanding?  ", Integer) { |q| q.in = 0..@withPlayer.funds }

      @offerFunds = ask("How much money are you offering?  ", Integer) { |q| q.in = 0..int1.game.currentPlayer.funds }

      @confirm = agree("Are you sure you wish to trade with #{@withPlayer.name.capitalize}? (y/n)  ", character = nil)

      int1.game.trade(@withPlayer, @offerFunds, @demandFunds, @offerFields, @demandFields, @confirm)

      if @confirm
        puts "\nTrade completed!"
      else
        puts "\nTrade aborted!"
      end

    end # end trade choice

    unless int1.game.currentPlayer.colorsOwned.empty?

      fieldsForBuilding = [] # will store names of fields on which a player can build
      int1.game.currentPlayer.colorsOwned.each do |color|

        hasMostHouses = color.fields.max_by { |field| field.houses }

        unless (color.fields.any? { |field| field.underMortgage })
          unless (color.fields.all? { |field| field.houses == color.fields[0].houses })  # because you can't build houses on one field unless you have at least as many houses on other fields (monopoly rule)
            color.fields.each do |field|
              if (field.houses < hasMostHouses.houses) && (field.houses < 5) && (field.houseCost <= int1.game.currentPlayer.funds)
                fieldsForBuilding << field.fieldName
              end
            end
          else
            color.fields.each do |field|
              if (field.houses < 5) && (field.houseCost <= int1.game.currentPlayer.funds)
                fieldsForBuilding << field.fieldName
              end
            end
          end
        end

      end

      menu.choice("Build house or hotel") do

        choose do |menu|

          menu.choice("Cancel") do
            print ""
          end

          menu.choices(*fieldsForBuilding) do |chosen|
            int1.game.currentPlayer.fieldsOwned.each do |field|
              if field.fieldName == chosen
                field.addHouse(int1.game.currentPlayer)
              end
            end
          end

        end

      end

    end

    if int1.game.currentPlayer.colorsOwned.any? { |color| color.hasHouses? }

      menu.choice("Sell house") do

        fieldsForRemovingHouses = []

        int1.game.currentPlayer.colorsOwned.each do |color|

          hasLeastHouses = color.fields.min_by { |field| field.houses }

          unless (color.fields.all? { |field| field.houses == color.fields[0].houses })  # because you can't sell houses on a field that has fewer houses than other fields (monopoly rule)
            color.fields.each do |field|
              if (field.houses > hasLeastHouses.houses)
                fieldsForRemovingHouses << field.fieldName
              end
            end
          else
            color.fields.each do |field|
              if field.houses > 0
                fieldsForRemovingHouses << field.fieldName
              end
            end
          end

        end

        say("Where do you want to sell houses?")

        choose do |menu|

          menu.choice("Cancel") do
            print ""
          end

          menu.choices(*fieldsForRemovingHouses) do |chosen|
            int1.game.currentPlayer.fieldsOwned.each do |field|
              if field.fieldName == chosen
                field.removeHouse(int1.game.currentPlayer)
              end
            end
          end

        end

      end

    end

    menu.choice("Show player stats") do

      say("Whose stats do you want to see?")

      choose do |menu|

        menu.choice("My Stats") do
          int1.showPlayerStats(int1.game.currentPlayer)
        end

        otherPlayers = int1.players.select { |player| player != int1.game.currentPlayer }.collect { |player| player.name }

        menu.choices(*otherPlayers) do |chosen|

          int1.players.each do |player|
            if chosen == player.name
              int1.showPlayerStats(player)
            end
          end

        end

      end

    end

    menu.choice("Show field stats") do

      say("Which field do you want to check?")

      choose do |menu|

        fieldNames = int1.fields.select { |field| (field.property? || field.station? || field.utility?) }.collect { |field| field.fieldName }

        menu.choices(*fieldNames) do |chosen|
          int1.showFieldStats(chosen)
        end

      end

    end

    if (!int1.game.currentPlayer.fieldsOwned.empty?) or (!int1.game.currentPlayer.colorsOwned.all? { |color| color.hasHouses? })  # prevents showing the mortgage option if there are no fields to mortgage

      menu.choice("Put under mortgage") do

        say("Which field do you want to put under mortgage?")
        choose do |menu|

          fieldsForMortgage = []

          unless int1.game.currentPlayer.colorsOwned.empty?
            int1.game.currentPlayer.colorsOwned.each do |color|
              unless color.hasHouses?
                color.fields.each do |field|
                  fieldsForMortgage << field
                end
              end
            end
          else
            fieldsForMortgage = int1.game.currentPlayer.fieldsOwned
          end

          fieldsForMortgage = fieldsForMortgage.select { |field| !field.underMortgage }.collect { |field| field.fieldName }

          menu.choice("Cancel") do
            print ""
          end

          menu.choices(*fieldsForMortgage) do |chosen|
            int1.game.currentPlayer.fieldsOwned.each do |field|
              if field.fieldName == chosen
                int1.game.putUnderMortgage(field)
              end
            end
          end

        end

      end

    end

    unless int1.game.currentPlayer.fieldsUnderMortgage.empty?

      menu.choice("Pay mortgage") do

        say("Which field do you want to pay mortgage for?")
        choose do |menu|

          fieldsForMortgage = []
          int1.game.currentPlayer.fieldsUnderMortgage.each do |field|
            fieldsForMortgage << field.fieldName
          end

          menu.choice("Cancel") do
            print ""
          end

          menu.choices(*fieldsForMortgage) do |chosen|
            int1.game.currentPlayer.fieldsUnderMortgage.each do |field|
              if field.fieldName == chosen
                int1.game.payMortgage(field)
              end
            end
          end

        end

      end

    end

    if int1.game.currentPlayer.inJail?
      menu.choice("Use Get out of jail free card") do
        int1.game.currentPlayer.useJailCard
      end
    end

    menu.choice("Show map") do
      puts int1.showMap
    end

    menu.choice("Quit game") do
      @gameOver = agree("Are you sure? (y/n)", character=nil)
    end

  end # end main menu

end # end main loop
