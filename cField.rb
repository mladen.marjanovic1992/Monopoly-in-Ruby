class Field

  attr_reader :fieldName, :fieldColor, :fieldType, :price, :rent, :houseCost, :owner, :mortgage
  attr_accessor :currentRent, :underMortgage, :houses

  def initialize(fieldType, fieldName, fieldColor, price, rent, houseCost) # price default is 1000000 to prevent the possibility of purchasing non-property fields
    @fieldType = fieldType
    @fieldName = fieldName
    @fieldColor = fieldColor
    @owner = nil
    @underMortgage = false
    @price = price
    @rent = rent
    @rent != nil ? @currentRent = @rent[0] : @currentRent = 0
    @price != nil ? @mortgage = @price / 2 : @mortgage = 0
    @houseCost = houseCost
    @houses = 0
  end

  def owned?
    @owner != nil ? true : false
  end

  def property?
    @fieldType == :property ? true : false
  end

  def station?
    @fieldType == :station ? true : false
  end

  def utility?
    @fieldType == :utility ? true : false
  end

  def go_to_jail?
    @fieldType == :go_to_jail ? true : false
  end

  def jail?
    @fieldType == :jail ? true : false
  end

  def setOwner(player) # player is an object of cPlayer class
    @owner = player
  end

  def addHouse(player)
    @houses += 1
    player.changeFunds(-@houseCost)
    changeRent(1)
  end

  def removeHouse(player)
    @houses -= 1
    player.changeFunds((@houseCost/2))
    changeRent(-1)
  end

  def changeRent(increment) # change is a positive or negative integer depending on whether you are adding or selling a house
    @currentRent = @rent[(@rent.index(@currentRent) + increment)]
  end

end
