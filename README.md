Console Monopoly game built in Ruby.
Follows standard Monopoly rules.

Required gems: Highline, Text-table

To display map correctly in the console, set console width to 200.

To start the game open file cInterface.rb