require 'yaml'
require 'text-table'
require './cField.rb'
require './cColor.rb'

class Map

  attr_reader :fields, :colors, :data, :board, :lastRow
  attr_accessor :colors # needed to populate the colors array from cInterface

  def initialize

    @fields = []
    @colors = [] # we have to decide how to populate this array. It will be used in cPlayer - colorsOwned method

    YAML::load_file(File.join(__dir__, 'map.yml')).each do |x| # adds fields from yaml file to @fields array
      @fields << Field.new(x["fieldType"], x["fieldName"], x["fieldColor"], x["price"], x["rent"], x["houseCost"])
    end

    # @board is a printable version of the map
    @board = Text::Table.new  # creates new table using text-table gem
    @board.rows << (@fields[0..10].collect { |x| x.fieldName }) # first row
    @board.rows << :separator
    # begin -> middle rows
    a = 39
    b = 11
    9.times do
      @board.rows << ["---------------", {:value => "", :colspan => 9}, "----------------------"] # adds '*' between rows to improve visibility
      @board.rows << [@fields[a].fieldName, {:value => "", :colspan => 9}, @fields[b].fieldName]
      a -= 1
      b += 1
    end
    @board.rows << ["---------------", {:value => "", :colspan => 9}, "----------------------"] # adds '*' before last row
    # end -> middle rows
    @board.rows << :separator
    @board.rows << (@fields[20..30].reverse.collect { |x| x.fieldName }) # last row
  end

end
