
class Game

  attr_reader :currentPlayer, :players, :map, :bankruptedByPlayer, :bankruptedByBank, :overpaid

  def initialize(map, players)
    @map = map
    @players = players
    @currentPlayer = players[0]
    @jailExitCardsGlobal = 2
    @dice1 = 0
    @dice2 = 0
    @chanceCardsPulled = 0
    @treasuryCardsPulled = 0
    @chanceCards = []
    @treasuryCards = []
    YAML::load_file(File.join(__dir__, 'chancecards.yml')).each do |x|
      @chanceCards << x
    end
    @chanceCards.shuffle!
    YAML::load_file(File.join(__dir__, 'treasurycards.yml')).each do |x|
      @treasuryCards << x
    end
    @treasuryCards.shuffle!
  end

  def roll
    @dice1 = rand(1..6)
    @dice2 = rand(1..6)
    puts "\nYou rolled a #{@dice1} and a #{@dice2}!"
    return (@dice1 + @dice2)
  end

  def rolledDouble?
    @dice1 == @dice2 ? true : false
  end

  def payRent(field, propertyOwner)
    unless (!field.owned?) || (field.owner == @currentPlayer) || (@currentPlayer.position.underMortgage)
      @currentPlayer.changeFunds(-field.currentRent)
      propertyOwner.changeFunds(field.currentRent)
    end
  end

  def payUtility(chanceCardModifier=1) # the modifier will be 2.5 because if a player gets the chance card go to the nearest utility he has to pay roll * 10 even if the owner doesn't own both utilities!
    if @currentPlayer.position.owned? && (@currentPlayer.position.owner != @currentPlayer)
      rolled = roll
      if @currentPlayer.fieldsOwned.select { |field| field.utility? }.length == 2
        @currentPlayer.changeFunds((-rolled * 10))
        @currentPlayer.position.owner.changeFunds((rolled * 10))
      else
        @currentPlayer.changeFunds((-rolled * 4 * chanceCardModifier))
        @currentPlayer.position.owner.changeFunds((rolled * 4 * chanceCardModifier))
      end
    end
  end

  def effect
    case @currentPlayer.position.fieldType
    when :property
      payRent(@currentPlayer.position, @currentPlayer.position.owner)
    when :station
      payRent(@currentPlayer.position, @currentPlayer.position.owner)
    when :utility
      payUtility()
    when :propertyTax
      @currentPlayer.changeFunds(-100)
    when :incomeTax
      @currentPlayer.changeFunds(-200)
    when :parking
      puts "At least you don't have to pay anything..."
    when :jail
      puts "Just visiting..."
    when :go_to_jail
      goToJail
    when :chance
      chanceCardEffect
    when :treasury
      treasuryCardEffect
    end
  end

  def chanceCardEffect
    case @chanceCards[@chanceCardsPulled][0]
    when "payment"
      @currentPlayer.changeFunds(@chanceCards[@chanceCardsPulled][2].to_i)
      puts @chanceCards[@chanceCardsPulled][1]
    when "goto"
      puts "Go to #{@map.fields[@chanceCards[@chanceCardsPulled][1].to_i].fieldName}"
      @currentPlayer.setPosition(@map.fields[@chanceCards[@chanceCardsPulled][1].to_i])
      effect
    when "otherPlayers"
      @currentPlayer.changeFunds(4 * @chanceCards[@chanceCardsPulled][1].to_i)
      @players.select { |player| player != @currentPlayer }.each { |player| player.changeFunds(-(@chanceCards[@chanceCardsPulled][1].to_i)) }
    when "getOutOfJail"
      unless @jailExitCardsGlobal == 0
        getOutOfJailCard
      else
        @chanceCardsPulled != (@chanceCards.length - 1) ? @chanceCardsPulled += 1 : @chanceCardsPulled = 0
        chanceCardEffect
      end
    when "houses"
      renovation(@chanceCards[@chanceCardsPulled][1].to_i, @chanceCards[@chanceCardsPulled][2].to_i) # add bancrupcy by bank
    when "goToNearest"
      goToNearest(@chanceCards[@chanceCardsPulled][1].to_sym)
    when "back3Fields"
      movePlayer(-3)
      effect
    end
    @chanceCardsPulled != (@chanceCards.length - 1) ? @chanceCardsPulled += 1 : @chanceCardsPulled = 0
  end

  def treasuryCardEffect
    case @treasuryCards[@treasuryCardsPulled][0]
    when "payment"
      @currentPlayer.changeFunds(@treasuryCards[@treasuryCardsPulled][2].to_i)
      puts @treasuryCards[@treasuryCardsPulled][1]
    when "goto"
      puts "Go to #{@map.fields[@treasuryCards[@treasuryCardsPulled][1].to_i].fieldName}"
      @currentPlayer.setPosition(@map.fields[@treasuryCards[@treasuryCardsPulled][1].to_i])
      effect
    when "otherPlayers"
      @currentPlayer.changeFunds(4 * @treasuryCards[@treasuryCardsPulled][1].to_i)
      @players.select { |player| player != @currentPlayer }.each { |player| player.changeFunds(-(@treasuryCards[@treasuryCardsPulled][1].to_i)) }
    when "getOutOfJail"
      unless @jailExitCardsGlobal == 0
        getOutOfJailCard
      else
        @treasuryCardsPulled != (@treasuryCards.length - 1) ? @treasuryCardsPulled += 1 : @treasuryCardsPulled = 0
        chanceCardEffect
      end
    when "houses"
      renovation(@treasuryCards[@treasuryCardsPulled][1].to_i, @treasuryCards[@treasuryCardsPulled][2].to_i) # add bancrupcy by bank
    end
    @treasuryCardsPulled != (@treasuryCards.length - 1) ? @treasuryCardsPulled += 1 : @treasuryCardsPulled = 0
  end

  def nextPlayer
    if @currentPlayer != @players.last
      @currentPlayer = @players[@players.index(@currentPlayer) + 1]
    else
      @currentPlayer = @players[0] # if the current player is the last player in the array, the next player will be the first player in the array
    end
    puts "\n#{@currentPlayer.name.capitalize}\'s move\n"
    puts "Position: #{@currentPlayer.position.fieldName}"
  end

  def movePlayer(rolled)
    unless @map.fields.index(@currentPlayer.position) + rolled > 39
      @currentPlayer.setPosition(@map.fields[@map.fields.index(@currentPlayer.position) + rolled])
    else
      @currentPlayer.setPosition(@map.fields[@map.fields.index(@currentPlayer.position) - 40 + rolled])
      @currentPlayer.changeFunds(200) # gets 200 when he crosses the start field
    end
    unless rolled == 1 # to prevent printing position for every field on goToNearest chanceCard
      puts "Position: #{@currentPlayer.position.fieldName}\n"
    end
  end

  def buyOrAuction(player, field, cost)

      field.setOwner(player)
      player.addField(field)
      player.changeFunds(-cost)

      @map.colors.each do |color|
        if (color.fields - player.fieldsOwned).empty? # checks if the player owns all fields of the same color
          color.setOwner(player) # sets player as owner of color if the above criteria is met
          player.addColor(color)
        end
      end

      puts "\n#{field.fieldName} bought by #{player.name.capitalize} for #{cost}$."

  end

  def trade(withPlayer, offerFunds, demandFunds, offerFields, demandFields, confirm) # confirm is a boolean

    if confirm

      withPlayer.changeFunds(offerFunds - demandFunds)
      @currentPlayer.changeFunds(demandFunds - offerFunds)

      offerFields.each do |field|
        withPlayer.addField(field)
        field.setOwner(withPlayer)
      end

      offerFields.each do |field| # has to be separate because removeField deletes a field from offerFields and doesn't transfer all fields to new owner
        @currentPlayer.removeField(field)
      end

      demandFields.each do |field|
        @currentPlayer.addField(field)
        field.setOwner(@currentPlayer)
      end

      demandFields.each do |field| # same issue as offerFields
        withPlayer.removeField(field)
      end

      @map.colors.each do |color|
        if color.ownedBy?(@currentPlayer)
          withPlayer.removeColor(color)
          color.setOwner(@currentPlayer)
          @currentPlayer.addColor(color)
        elsif color.ownedBy?(withPlayer)
          @currentPlayer.removeColor(color)
          color.setOwner(withPlayer)
          withPlayer.addColor(color)
        end
      end

    end

  end

  def putUnderMortgage(field)
    @currentPlayer.changeFunds(field.mortgage)
    field.underMortgage = true
    @currentPlayer.fieldsUnderMortgage << field
  end

  def payMortgage(field, confirm)
    unless (@currentPlayer.funds < (field.mortgage * 1.1)) || (!confirm)
      @currentPlayer.changeFunds(-(field.mortgage * 1.1))
      field.underMortgage = false
      @currentPlayer.fieldsUnderMortgage.delete(field)
    end
  end

  def bankrupt

    puts "You are bankrupt!"

    @overpaid = -(@currentPlayer.funds) - @currentPlayer.remainingAssets # because the property owner recieved the full rent, he must "return" the difference between the full rent and the bankrupt player's remaining funds

    @currentPlayer.fieldsOwned.each do |field| # removes houses from all fields owned by the bankrupt player
      field.houses = 0
    end

    yield

    previousPlayerIndex = @players.index(@currentPlayer) - 1
    @players.delete(@currentPlayer)
    @currentPlayer = @players[previousPlayerIndex]

  end

  # chance card methods (some chance cards will use exesting methods, like changeFunds etc.)

  def getOutOfJailCard
    @currentPlayer.jail_exit_cards += 1
    @jailExitCardsGlobal -= 1
    puts "Get out of jail free!"
  end

  def goToJail
    @currentPlayer.setPosition(@map.fields[10])
    @currentPlayer.rollsUntilOut = 3
    puts "Go to Jail!!"
  end

  def useJailCard
    @currentPlayer.jail_exit_cards -= 1
    @jailExitCardsGlobal += 1
    @currentPlayer.rollsUntilOut = 0
    puts "You are out of jail!"
  end

  def renovation(houseCost, hotelCost)
    total = 0
    @currentPlayer.fieldsOwned.select { |field| field.houses > 0 }.each do |field|
      field.houses < 5 ? total += (field.houses * houseCost ) : total += hotelCost
    end
    puts "You must renovate your houses and hotels! The total is #{total}."
    return total
  end

  def goToNearest(fieldType)
    until @currentPlayer.position.fieldType == fieldType
      movePlayer(1)
    end
    puts "Position: #{@currentPlayer.position.fieldName}\n"
    if fieldType == "station"
      2.times {effect}
    else
      payUtility(2.5)
    end
  end

end